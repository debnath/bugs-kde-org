# Setting up local testing environment

To setup the local environment, you can leverage [Docker
Compose](https://docs.docker.com/compose/). First, you need to install
[Docker](https://docs.docker.com/get-started/) on your machine.

After installation, run the following command from the `docker/` directory:

```bash
$ ./initial_setup.sh
```

In case this fails with `There was an error connecting to MySQL`, bring down the running containers and run the `initial_setup.sh` script again, like so:

```bash
$ docker-compose down
$ ./initial_setup.sh
```

It should succeed this time. This should set up the environment for you. You can then navigate to
http://localhost:8080/ in the browser to see the local interface of Bugzilla
(Make sure that you're using the `http://` and not the `https://`, otherwise
the browser could not let you pass).

To make changes in the local instance, such as creating or editing bugs, you
will have to log in into the tracker. The admin login and password for this
local deployment are:

Email: `admin@bugzilla.test`  
Password: `password01!`

This is specified within the `docker/checksetup_answers.txt` file.

Congratulations, now you can make changes to the source code and redeploy your
instance using docker compose! Sometimes it might be necessary to run the `./checksetup.pl` script inside the container to recompile various templates after you make changes. You can use the following command to do that:

```bash
$ docker-compose exec apache bash -c 'perl checksetup.pl && chown -R '$(id -u)' .'
```

Note that in command above, the `chown -R '$(id -u)' .` is necessary because the `./checksetup.pl` script will (among other things) try to fix file ownership and permissions inside the docker container, and will make `root` the owner of all the files in the repo. `chown -R '$(id -u)' .` changes the ownership back to you :)

To learn more, refer to the [official Docker documentation](https://docs.docker.com/).
